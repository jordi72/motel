import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class Parser {

	public static Character gender = null;
	
	public static void main(String[] args) {

		
		parseCSV();
	}
	
	@SuppressWarnings("resource")
	public static void parseCSV() {

		File names = new File("names.csv");
		File output = new File("englishNames.csv");
		
		try {
			Scanner in = new Scanner(names);
			
			String currentLine;
			String currentLineSplit[];
			String name = null;
			
			Map<String, Character> namesMap = new HashMap<String, Character>();
			
			while(in.hasNextLine()) {
				currentLine = in.nextLine();
				currentLineSplit = currentLine.split(",");
				name = currentLineSplit[1];
				if(currentLineSplit[3].equals("boy")) {
					gender = 'M';
				}
				else if(currentLineSplit[3].equals("girl")) {
					gender = 'F';
				}else {
					continue;
				}				
				namesMap.put(name, gender);
				System.out.println(currentLineSplit[1] + " " + currentLineSplit[3]);
				
			}

			
			PrintWriter writer = new PrintWriter(new FileWriter(output));
			
			
			
			Set<Entry<String, Character>> auxNames = namesMap.entrySet();
			for (Entry<String, Character> dada : auxNames) {
			    System.out.println(dada.getKey()+": "+dada.getValue());
			    for(int i = 0 ; i<dada.getKey().length(); i++) {
			    	writer.append(dada.getKey().charAt(i));
			    }
			    writer.append(';');
			    writer.append(dada.getValue());
			    writer.println();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
