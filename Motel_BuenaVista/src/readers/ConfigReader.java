package readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
	
	String connector, ip, port, user, password, aux, result = "";
	String db = "motel";
	
	
	public void readConf() throws FileNotFoundException {
		File file = new File("moteldb.conf");
		
		Properties prop = new Properties();
		InputStream in = null;
		
		try {
			in = new FileInputStream(file);
			
			prop.load(in);
			
			connector = prop.getProperty("connector");
			ip = prop.getProperty("ip");
			port = prop.getProperty("port");
			user = prop.getProperty("user");
			password = prop.getProperty("password");
			result = connector + "://" + ip + ":" + port + "/" + db;
		}catch(IOException ex) {
			ex.printStackTrace();
		}finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	public String getURL() {
		return this.result;
	}
	public String getUser() {
		return this.user;
	}
	public String getPassword() {
		return this.password;
	}
	public String getDB() {
		return this.db;
	}
	public String getIP() {
		return this.ip;
	}

}
