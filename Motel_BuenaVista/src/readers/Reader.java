package readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import models.Facilities;
import models.RoomType;
import models.Season;

public class Reader {

	public List<String> emails() throws FileNotFoundException {
		List<String> emails = new ArrayList<>();

		File file = new File("./data/emails.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String line = in.nextLine();
			String[] tempArray = line.split(";");
			emails.add(tempArray[0]);
		}
		in.close();
		return emails;
	}

	public List<String> nationalities() throws FileNotFoundException {
		List<String> nationalities = new ArrayList<>();

		File file = new File("./data/nationalities.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String line = in.nextLine();
			String[] tempArray = line.split(";");
			nationalities.add(tempArray[0]);
		}
		in.close();
		return nationalities;
	}

	public List<String> dot() throws FileNotFoundException {
		List<String> dot = new ArrayList<>();

		File file = new File("./data/nationalities.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String line = in.nextLine();
			String[] tempArray = line.split(";");
			dot.add(tempArray[1]);
		}
		in.close();
		return dot;
	}

	public List<String> spanishNames() throws FileNotFoundException {
		List<String> namesList = new ArrayList<>();

		File file = new File("./data/nombres.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			String name = tempArray[0];
			namesList.add(name);
		}
		in.close();
		return namesList;
	}

	public List<String> spanishLastNames() throws FileNotFoundException {
		List<String> namesList = new ArrayList<>();

		File file = new File("./data/apellidos.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			String name = tempArray[0];
			namesList.add(name);
		}
		in.close();
		return namesList;
	}

	public List<String> englishNames() throws FileNotFoundException {
		List<String> namesList = new ArrayList<>();

		File file = new File("./data/names.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			String name = tempArray[0];
			namesList.add(name);
		}
		in.close();
		return namesList;
	}

	public List<String> englishLastNames() throws FileNotFoundException {
		List<String> namesList = new ArrayList<>();

		File file = new File("./data/lastnames.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			String name = tempArray[0];
			namesList.add(name);
		}
		in.close();
		return namesList;
	}

	public List<Facilities> facilities() throws FileNotFoundException {
		List<Facilities> facilities = new ArrayList<>();

		File file = new File("./data/facilities.csv");

		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			Facilities fc = new Facilities(tempArray[0]);
			facilities.add(fc);
		}
		in.close();
		return facilities;
	}

	public List<Season> seasons() throws FileNotFoundException {

		File file = new File("./data/seasons.csv");

		Scanner in = new Scanner(file);
		List<Season> list = new ArrayList<>();
		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			String name = tempArray[0];
			String date = tempArray[1];
			String price = tempArray[2];
			list.add(new Season(name, date, price));

		}
		in.close();

		return list;
	}

	public List<RoomType> roomTypes() throws FileNotFoundException {

		File file = new File("./data/roomTypes.csv");

		Scanner in = new Scanner(file);
		List<RoomType> list = new ArrayList<>();
		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			String name = tempArray[0];
			int quantity = Integer.parseInt(tempArray[1]);
			Boolean shared = Boolean.valueOf(tempArray[2].toLowerCase());
			int priceBase = Integer.parseInt(tempArray[3]);
			list.add(new RoomType(name, quantity, shared, priceBase));

		}
		in.close();

		return list;
	}

	public List<String[]> roomTypesFacilities() throws FileNotFoundException {

		File file = new File("./data/roomTypesFacilities.csv");

		Scanner in = new Scanner(file);
		List<String[]> list = new ArrayList<>();
		while (in.hasNext()) {
			String tempString = in.nextLine();
			String[] tempArray = tempString.split(";");
			list.add(tempArray);

		}
		in.close();

		return list;
	}

	public String createTables() throws FileNotFoundException {
		String tables = "";

		File file = new File("./data/createTables.sql");

		Scanner in = new Scanner(file);
		while (in.hasNext()) {
			tables = tables + in.nextLine();
		}
		in.close();

		return tables;
	}
}
