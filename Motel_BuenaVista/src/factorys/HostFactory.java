package factorys;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import generators.DocGenerator;
import generators.NameGenerator;
import generators.NationalityGenerator;
import models.Customer;
import models.Host;

public class HostFactory {
	private static HostFactory instance;
	private Set<String> setB = new HashSet<>();
	Boolean duplicatedCustomer = false;
	Boolean duplicatedHost = false;

	/**
	 * Call NationalityGenerator, where going to call the class for new Random
	 * Nationality.
	 */
	private NationalityGenerator countries = new NationalityGenerator();
	/**
	 * Call NameGenerator. where create a new random Name and Last(s) names, need
	 * nationality.
	 */

	private NameGenerator names = new NameGenerator();
	/**
	 * Call DocGenerator for new document, Needs nationality too!
	 */
	private DocGenerator docs = new DocGenerator();

	/**
	 * Return the instance , the unique object of this class.
	 * 
	 * @return
	 */
	public static HostFactory getInstance() {
		if (instance == null)
			instance = new HostFactory();
		return instance;
	}

	/**
	 * Generate a array of people using a List.
	 * 
	 * @param list
	 *            Customers for getting all customers lists,remember, the hosts are
	 *            based fo customers.
	 * @return One list with all hosts.
	 */

	public List<Host> createHostsandCustomers(List<Customer> listCustomers) {
		List<Host> hosts = new ArrayList<>();
		for (Customer c : listCustomers) {
			duplicatedCustomer = false;
			duplicatedHost = false;
			int extraHosts = c.getnHosts();

			while (duplicatedCustomer == false) {
				Host hostCustomer = createHostCustomer(c);
				if (setB.contains(hostCustomer.getDocNumber())) {
				} else {
					setB.add(hostCustomer.getDocNumber());
					hosts.add(hostCustomer);
					duplicatedCustomer = true;

					for (int i = 0; i < extraHosts; i++) {
						duplicatedHost = false;
						while (duplicatedHost == false) {

							Host h = createHost();
							if (setB.contains(h.getDocNumber())) {
							} else {
								setB.add(h.getDocNumber());
								hosts.add(h);
								duplicatedHost = true;
							}
						}
					}

				}
			}
			
			listCustomers.indexOf(c);
		}
		return hosts;

	}

	/**
	 * Create a random host.
	 * 
	 * @return The generated host.
	 */
	public Host createHost() {
		Host h = new Host();
		// Obtenim nacionalitat
		String nationality = countries.getNewNationality();
		h.setNationality(nationality);
		h.setName(names.getName(nationality));
		h.setLastname(names.getLastName(nationality));
		String doctype = docs.getDoctype(nationality);
		h.setDocType(doctype);
		h.setDocNumber(docs.getNationality(nationality));
		return h;
	}

	/**
	 * Converter from customer to host.
	 * 
	 * @return The generated host.
	 */
	public Host createHostCustomer(Customer c) {
		Host h = new Host();
		// Obtenim nacionalitat
		h.setNationality(c.getNationality());
		h.setName(c.getName());
		h.setLastname(c.getLastname());
		String doctype = docs.getDoctype(c.getNationality());
		h.setDocType(doctype);
		h.setDocNumber(docs.getNationality(c.getNationality()));
		return h;
	}

}
