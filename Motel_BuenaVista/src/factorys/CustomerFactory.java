package factorys;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import generators.EmailGenerator;
import generators.NameGenerator;
import generators.NationalityGenerator;
import generators.PhoneGenerator;
import models.Customer;

public class CustomerFactory {

	private static CustomerFactory instance;
	private Set<String> setA = new HashSet<String>();
	/**
	 * Call NationalityGenerator, where going to call the class for new Random
	 * Nationality.
	 */
	private NationalityGenerator countries = new NationalityGenerator();
	/**
	 * Call NameGenerator. where create a new random Name and Last(s) names, need
	 * nationality.
	 */

	private NameGenerator names = new NameGenerator();
	/**
	 * Call EmailGeneator for new email
	 */
	private EmailGenerator emails = new EmailGenerator();
	/**
	 * Call PhomeGenerator for new phone.
	 */

	private PhoneGenerator phones = new PhoneGenerator();

	/**
	 * Return the instance , the unique object of this class.
	 * 
	 * @return
	 */
	public static CustomerFactory getInstance() {
		if (instance == null)
			instance = new CustomerFactory();
		return instance;
	}

	private Boolean duplicatedCustomer = false;

	/**
	 * Generate a array of N random customers
	 * 
	 * @param n
	 *            The quanity of customers to create
	 * @return One list full of customers.
	 */

	public List<Customer> createCustomers(int n) {

		List<Customer> customers = new ArrayList<Customer>();

		for (int i = 0; i < n; i++) {
			duplicatedCustomer = false;
			while (duplicatedCustomer == false) {
				Customer c = createCustomer();
				if (setA.contains(c.getEmail())) {

				} else {
					setA.add(c.getEmail());
					customers.add(c);
					duplicatedCustomer = true;
				}
			}
		}

		return customers;
	}

	/**
	 * Create a random customer
	 * 
	 * @return The generated customer.
	 */
	public Customer createCustomer() {
		Customer c = new Customer();
		String nationality = countries.getNewNationality();
		c.setNationality(nationality);
		c.setName(names.getName(nationality));
		c.setLastname(names.getLastName(nationality));
		c.setDot(names.getDomain(nationality));
		c.setEmail(emails.getEmail(c));
		c.setPhoneNumber(phones.getPhone(c));
		return c;
	}

}
