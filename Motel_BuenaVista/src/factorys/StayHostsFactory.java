package factorys;

import java.util.ArrayList;
import java.util.List;

import jdbc.BookingJDBC;
import models.Booking;
import models.StayHost;

public class StayHostsFactory {

	List<Booking> listOfBooking;
	
	public StayHostsFactory() {
		recuperateBooking();
	}
	public void recuperateBooking() {
		BookingJDBC bjdbc = new BookingJDBC();
		listOfBooking = bjdbc.selectBookingsNotReserved();
	}
	
	public List<StayHost> createStayHosts() {
		int counter = 1;
		List<StayHost> lsh = new ArrayList<>();
		for(Booking b : listOfBooking) {
			int stayId = listOfBooking.indexOf(b) + 1;
			for(int i = 0 ; i<=b.getNhosts(); i++) {
				StayHost sh = new StayHost(stayId, counter);
				counter++;
				lsh.add(sh);		
			}
		}
		return lsh;
	}
	
}
