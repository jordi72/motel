package factorys;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jdbc.BookingJDBC;
import jdbc.HostJDBC;
import jdbc.PriceSeasonsJDBC;
import jdbc.RoomJDBC;
import jdbc.RoomTypeJDBC;
import jdbc.SeasonsJDBC;
import jdbc.StayJDBC;
import models.Booking;
import models.Customer;
import models.Room;
import models.RoomType;
import models.Season;
import models.Stay;

public class BookingFactory {

	private static BookingFactory instance;

	public static BookingFactory getInstance() {
		if (instance == null)
			instance = new BookingFactory();
		return instance;
	}

	RoomJDBC rjdbc = new RoomJDBC();

	RoomTypeJDBC rtjdbc = new RoomTypeJDBC();

	BookingJDBC bjdbc = new BookingJDBC();

	SeasonsJDBC sjdbc = new SeasonsJDBC();

	PriceSeasonsJDBC psjdbc = new PriceSeasonsJDBC();

	StayJDBC stayjdbc = new StayJDBC();

	public List<Booking> generateBooking(List<Customer> customers) throws SQLException {
		System.out.println("Generating Bookings...");
		List<Customer> clist = new ArrayList<>();
		List<Booking> bookingList = new ArrayList<>();

		String reserved = "Reserved";
		String canceled = "Cancelled";
		String card = "Card";
		String cash = "Cash";

		Customer c;
		Season s = null;
		RoomType rt = null;
		LocalDate baseDate = LocalDate.of(LocalDate.now().getYear(), 1, 1);

		boolean outOfCustomer = false;

		while (!outOfCustomer) {

			List<Room> roomList = rjdbc.selectEmptyRooms();

			LocalDate tempDate = LocalDate.of(LocalDate.now().getYear(), 1, 1);
			if (!baseDate.equals(tempDate)) {

				List<Booking> toCheckoutBookings = bjdbc.selectBookingsbyCheckOut(baseDate);
				int auxRoomToCheckout;
				String customerEmail;
				Stay auxStayToCheckout;

				for (Booking b : toCheckoutBookings) {
					if (b.getState().equals(canceled)) {
						
					} else {
						customerEmail = b.getCustomerEmail();
						auxStayToCheckout = stayjdbc.selectOneStaybyCustomerEmail(customerEmail);
						auxRoomToCheckout = auxStayToCheckout.getRoomNumber();
						rjdbc.updateRoomStatus(auxRoomToCheckout, true);
					}
				}
			}

			double price;

			for (Room r : roomList) {
				if (!customers.isEmpty()) {
					c = customers.get(0);
					c.setnHosts(current().nextInt(3) + 1);
					s = sjdbc.getSeason(baseDate);

					rt = rtjdbc.selectOneRoomTypebyId(r.getRoomType());

					price = psjdbc.getBookingPrice(rt.getId(), s.getId());

					String state = current().nextDouble(1) > 0.1 ? reserved : canceled;
					String paymentType = current().nextDouble(1) <= 0.5 ? card : cash;
					
					// Creating Booking
					Booking booking = new Booking(baseDate.minusDays(current().nextInt(10)), c.getEmail(), rt.getId(),
							baseDate, baseDate.plusDays(current().nextInt(8) + 1), price, state, c.getnHosts());
					bjdbc.insertBooking(booking);
					if (!booking.getState().equals(canceled)) {
						
						// Creating Stay based on Booking
						Stay stay = new Stay(booking.getCheckIn(), r.getRoomNumber(), booking.getCheckOut(),
								price * booking.getNhosts(), paymentType, booking.getReservationDateTime(),
								booking.getCheckOut(), c.getEmail());
						stayjdbc.insertStay(stay);
						rjdbc.updateRoomStatus(r.getRoomNumber(), false);
						clist.add(c);
					}
					customers.remove(0);
					c = null;
					bookingList.add(booking);

				} else {
					outOfCustomer = true;
				}
			}
			baseDate = baseDate.plusDays(1);
			System.out.println("Bookings & Stays of -> " + baseDate.toString());
		}
		System.out.println("Bookings & Stays Done!\n");
		createHosts(clist);
		return bookingList;
	}
	
	public void createHosts(List<Customer> customers) {
			HostFactory hf = new HostFactory();
			HostJDBC hjdbc = new HostJDBC();
			hjdbc.instertHosts(hf.createHostsandCustomers(customers));
		
	}
}
