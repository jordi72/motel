package factorys;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.util.ArrayList;
import java.util.List;

import jdbc.RoomTypeJDBC;
import models.Room;

public class RoomFactory {

	// Need JDBC for set the RoomTypeId
	
	
	public List<Room> createRooms(int n){
		List<Room> lr = new ArrayList<Room>();
		
		for(int i = 0 ;i<n;i++) {
			lr.add(createRoom());
		}
		return lr;
		
	}
	public Room createRoom() {
		Room r = new Room();	
		r.setEmpty(true);
		r.setRoomType(getRandomRoomType());
		return r;
	}
	
	public int getRandomRoomType() {
		RoomTypeJDBC roomtypejdbc = new RoomTypeJDBC();
		roomtypejdbc.selectRoomTypes().size();
		return current().nextInt(roomtypejdbc.selectRoomTypes().size()) + 1 ;
		
	}
	
}
