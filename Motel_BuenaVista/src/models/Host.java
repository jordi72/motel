package models;

public class Host {
	
	String name;
	String lastname;
	String nationality;
	String docType;
	String docNumber;
	int id;
	
	public Host() {

	}

	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Host(String name, String lastname, String nationality, String docType, String docNumber) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.nationality = nationality;
		this.docType = docType;
		this.docNumber = docNumber;
	}


	//GETTERS AND SETTERS
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	
	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	

	public String getDocType() {
		return docType;
	}


	public void setDocType(String docType) {
		this.docType = docType;
	}


	public String getDocNumber() {
		return docNumber;
	}


	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}


	@Override
	public String toString() {
		return "Host [name=" + name + ", lastname=" + lastname + ", nationality=" + nationality + ", docType=" + docType
				+ ", docNumber=" + docNumber + "]";
	}
	

}
