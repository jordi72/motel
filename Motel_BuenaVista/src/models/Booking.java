package models;

import java.time.LocalDate;

public class Booking {
	int id;
	LocalDate reservationDateTime;
	String customerEmail;
	int roomTypeId;
	LocalDate checkIn;
	LocalDate checkOut;
	Double price;
	String state;
	int nhosts;
	

	public Booking(LocalDate reservationDateTime, String customerEmail, int roomTypeId, LocalDate checkIn, LocalDate checkOut,
			Double price, String state, int nhosts) {
		super();
		this.reservationDateTime = reservationDateTime;
		this.customerEmail = customerEmail;
		this.roomTypeId = roomTypeId;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.price = price;
		this.state = state;
		this.nhosts = nhosts;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getReservationDateTime() {
		return reservationDateTime;
	}
	public void setReservationDateTime(LocalDate reservationDateTime) {
		this.reservationDateTime = reservationDateTime;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public int getRoomTypeId() {
		return roomTypeId;
	}
	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}
	public LocalDate getCheckIn() {
		return checkIn;
	}
	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}
	public LocalDate getCheckOut() {
		return checkOut;
	}
	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getNhosts() {
		return nhosts;
	}
	public void setNhosts(int nhosts) {
		this.nhosts = nhosts;
	}
	
	@Override
	public String toString() {
		return "Booking [id=" + id + ", reservationDateTime=" + reservationDateTime + ", customerEmail=" + customerEmail
				+ ", roomTypeId=" + roomTypeId + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", price=" + price
				+ ", state=" + state + ", nhosts=" + nhosts + "]";
	}
	
}
