package models;

public class Customer {
	int id;
	String name;
	String lastname;
	String nationality;
	String phoneNumber;
	String dot;
	String email;
	int nHosts;
	
	public Customer() {

	}

	
	public Customer( String name, String lastname, String phoneNumber,String email,String nationality) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.nationality = nationality;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	//GETTERS AND SETTERS
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	
	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getDot() {
		return dot;
	}


	public void setDot(String dot) {
		this.dot = dot;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getnHosts() {
		return nHosts;
	}


	public void setnHosts(int nHosts) {
		this.nHosts = nHosts;
	}


	@Override
	public String toString() {
		return "Customer [name=" + name + ", lastname=" + lastname + ", nationality=" + nationality + ", phoneNumber="
				+ phoneNumber + ", dot=" + dot + ", email=" + email + ", nHosts=" + nHosts + "]";
	}
}
