package models;

public class RoomTypeFacilities {
	
	int id;
	int facilityId;
	int roomTypeId;
	public RoomTypeFacilities(int roomTypeId, int facilityId) {
		super();
		this.facilityId = facilityId;
		this.roomTypeId = roomTypeId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFacilityId() {
		return facilityId;
	}
	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}
	public int getRoomTypeId() {
		return roomTypeId;
	}
	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}
	
	

}
