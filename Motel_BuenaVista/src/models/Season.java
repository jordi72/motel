package models;

import java.text.ParseException;
import java.time.LocalDate;

public class Season {

	int id;
	LocalDate date = null;
	String stringDate;
	String seasonName = null;
	double seasonPrice;

	public Season(String name, String date, String price) {
		try {
			setSeasonName(name);
			setDate(date);
			this.stringDate = date;
			setseasonPrice(price);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setDate(String s) throws ParseException {
		
		String [] temp = s.split("-");
		
		 date = LocalDate.of(2000, Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
	}

	public void setSeasonName(String name) {
		this.seasonName = name;
	}

	public void setId(int id) {
		this.id = id;
		
	}
	public int getId() {
		return this.id;
	}
	public void setseasonPrice(String price) {
		this.seasonPrice = Double.parseDouble(price);
	}

	public LocalDate getLocalDate() {
		return date;
	}

	public LocalDate getData() {
		return date;
	}
	
	public void setLocalDate(LocalDate date) {
		this.date = date;
	}

	public double getSeasonPrice() {
		return seasonPrice;
	}

	public void setSeasonPrice(double seasonPrice) {
		this.seasonPrice = seasonPrice;
	}

	public String getSeasonName() {
		return seasonName;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

	@Override
	public String toString() {
		return "Season [calendar=" + date + ", seasonName=" + seasonName + ", seasonPrice=" + seasonPrice + "]";
	}
}
