package models;

public class RoomType {

	String name;
	int capacity;
	Boolean shared;
	int priceBase;
	int id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RoomType(String name, int capacity, Boolean shared, int priceBase) {
		super();
		this.name = name;
		this.capacity = capacity;
		this.shared = shared;
		this.priceBase = priceBase;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public Boolean getShared() {
		return shared;
	}
	public void setShared(Boolean shared) {
		this.shared = shared;
	}
	public int getPriceBase() {
		return priceBase;
	}
	public void setPriceBase(int priceBase) {
		this.priceBase = priceBase;
	}
	
}
