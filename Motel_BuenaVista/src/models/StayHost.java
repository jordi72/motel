package models;

public class StayHost {
	int stayId;
	int HostId;

	public int getStayId() {
		return stayId;
	}

	public void setStayId(int stayId) {
		this.stayId = stayId;
	}

	public int getHostId() {
		return HostId;
	}

	public void setHostId(int hostId) {
		HostId = hostId;
	}

	public StayHost(int stayId, int hostId) {
		super();
		this.stayId = stayId;
		HostId = hostId;
	}

}
