package models;

import jdbc.RoomTypeJDBC;
import jdbc.SeasonsJDBC;

public class PriceSeason {

	int id;
	int seasonId;
	int roomTypeId;
	double price;
	Season s;
	RoomType rt;
	

	public PriceSeason(int roomTypeId, int seasonId) {
		super();
		this.seasonId = seasonId;
		this.roomTypeId = roomTypeId;
		getSeason();
		getRoomType();
		calculatePrice();
	}

	public void getSeason() {
		SeasonsJDBC sjdbc = new SeasonsJDBC();
		s = sjdbc.selectOneSeasonById(seasonId);
	}
	
	public void getRoomType() {
		RoomTypeJDBC rtjdbc = new RoomTypeJDBC();
		rt = rtjdbc.selectOneRoomTypebyId(roomTypeId);
		
	}
	
	public void calculatePrice() {
		price = rt.getPriceBase() * s.getSeasonPrice();
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(int seasonId) {
		this.seasonId = seasonId;
	}

	public int getRoomTypeId() {
		return roomTypeId;
	}

	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
