package models;

import java.time.LocalDate;

public class Stay {

	int id;
	LocalDate checkIn;
	int roomNumber;
	LocalDate checkOut;
	double totalPrice;
	String paymanetType;
	LocalDate reservationDateTime;
	LocalDate PaymentDateTime;
	String customerEmail;

	public Stay(LocalDate checkIn, int roomNumber, LocalDate checkOut, double totalPrice, String paymanetType,
			LocalDate reservationDateTime, LocalDate paymentDateTime, String customerEmail) {
		super();
		this.checkIn = checkIn;
		this.roomNumber = roomNumber;
		this.checkOut = checkOut;
		this.totalPrice = totalPrice;
		this.paymanetType = paymanetType;
		this.reservationDateTime = reservationDateTime;
		PaymentDateTime = paymentDateTime;
		this.customerEmail = customerEmail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public LocalDate getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPaymanetType() {
		return paymanetType;
	}

	public void setPaymanetType(String paymanetType) {
		this.paymanetType = paymanetType;
	}

	public LocalDate getReservationDateTime() {
		return reservationDateTime;
	}

	public void setReservationDateTime(LocalDate reservationDateTime) {
		this.reservationDateTime = reservationDateTime;
	}

	public LocalDate getPaymentDateTime() {
		return PaymentDateTime;
	}

	public void setPaymentDateTime(LocalDate paymentDateTime) {
		PaymentDateTime = paymentDateTime;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	@Override
	public String toString() {
		return "Stay [id=" + id + ", checkIn=" + checkIn + ", roomNumber=" + roomNumber + ", checkOut=" + checkOut
				+ ", totalPrice=" + totalPrice + ", paymanetType=" + paymanetType + ", reservationDateTime="
				+ reservationDateTime + ", PaymentDateTime=" + PaymentDateTime + ", customerEmail=" + customerEmail
				+ "]";
	}
}
