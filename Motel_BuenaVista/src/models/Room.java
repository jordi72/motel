package models;

public class Room {

	int roomNumber;
	Boolean empty;
	int roomType;
	
	public Room() {
		
	}
	
	public Room(int roomNumber, Boolean empty, int roomType) {
		super();
		this.roomNumber = roomNumber;
		this.empty = empty;
		this.roomType = roomType;
	}
	
	
	public int getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	public Boolean getEmpty() {
		return empty;
	}
	public void setEmpty(Boolean empty) {
		this.empty = empty;
	}
	public int getRoomType() {
		return roomType;
	}
	public void setRoomType(int roomType) {
		this.roomType = roomType;
	}


	@Override
	public String toString() {
		return "Room [roomNumber=" + roomNumber + ", Empty=" + empty + ", roomType=" + roomType + "]";
	}



}
