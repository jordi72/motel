package db;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import readers.ConfigReader;

public class Dumper {

	public void dumpMysql(File f) throws InterruptedException, IOException, SQLException {
		File file = f;
		String osName = System.getProperty("os.name");
		if (isWindows(osName)) {
			System.out.println("The dump for Windows has not been implemented yet.");
			//dumpForWindows(file);
		} else {
			dumpForLinux(file);
		}
	}

	// Not working fine, working to fix it.
	public void dumpForWindows(File f) throws IOException, InterruptedException {
		// define backup file
		ConfigReader reader = new ConfigReader();
		reader.readConf();
		String dumpCommand = "cmd.exe/ " + getMysqlPath() + "\\" + "mysqldump.exe" + "\\"
				+ "--quick --lock-tables --user=" + reader.getUser() + "--password=" + reader.getPassword()
				+ " --databases" + reader.getDB();
		Runtime rt = Runtime.getRuntime();
		File test = f;
		PrintStream ps;

		try {
			Process child = rt.exec(dumpCommand);
			ps = new PrintStream(test);
			InputStream in = child.getInputStream();
			int ch;
			while ((ch = in.read()) != -1) {
				ps.write(ch);
			}
			System.out.println("Database Dumped.");
			InputStream err = child.getErrorStream();
			while ((ch = err.read()) != -1) {
				System.out.write(ch);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void dumpForLinux(File f) throws InterruptedException, IOException {
		ConfigReader reader = new ConfigReader();
		reader.readConf();

		String dumpCommand = "mysqldump " + reader.getDB() + " -h " + reader.getIP() + " -u " + reader.getUser() + " -p"
				+ reader.getPassword();
		Runtime rt = Runtime.getRuntime();
		File test = f;
		PrintStream ps;

		try {
			Process child = rt.exec(dumpCommand);
			ps = new PrintStream(test);
			InputStream in = child.getInputStream();
			int ch;
			while ((ch = in.read()) != -1) {
				ps.write(ch);
			}
			System.out.println("Database Dumped.");
			InputStream err = child.getErrorStream();
			while ((ch = err.read()) != -1) {
				System.out.write(ch);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public boolean isWindows(String os) {
		if (os.contains("Windows"))
			return true;
		return false;
	}

	public String getMysqlPath() {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT @@basedir";
		String basedir = null;
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					basedir = rs.getString("@@basedir");
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return basedir;
	}

}
