package db;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import readers.ConfigReader;

public class Connector {

	
	Connection connection;
	
	

	public Connector() {
		try {
			initConnection();
		} catch (FileNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}



	public Connector initConnection() throws FileNotFoundException, SQLException {
		ConfigReader conf = new ConfigReader();
			conf.readConf();
			 connection = DriverManager.getConnection(conf.getURL(), conf.getUser(), conf.getPassword());
		return this;
	}



	public Connection getConnection() {
		return connection;
	}



	public void setConnection(Connection connection) {
		this.connection = connection;
	}


	public void stopConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}
