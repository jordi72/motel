package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.Season;

public class SeasonsJDBC {

	public void insertSeasons(List<Season> seasons) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting Seasons");

		String sql = "INSERT INTO Seasons(StartingDay,Name,Price) VALUES (?,?,?)";

		try {
			PreparedStatement statement = conection.prepareStatement(sql);
			for (Season s : seasons) {
				statement.setString(1, s.getStringDate());
				statement.setString(2, s.getSeasonName());
				statement.setDouble(3, s.getSeasonPrice());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Season selectOneSeasonbyName(String name) {
		Season season = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Seasons WHERE Name LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setString(1, name);

			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					season = new Season(rs.getString("Name"), rs.getString("StartingDay"),
							Double.toString(rs.getDouble("Price")));
					season.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return season;

	}

	public Season selectOneSeasonById(int id) {
		Season season = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Seasons WHERE Id LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, id);

			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					season = new Season(rs.getString("Name"), rs.getString("StartingDay"),
							Double.toString(rs.getDouble("Price")));
					season.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return season;

	}

	public List<Season> selectSeasons() {
		List<Season> seasonsList = new ArrayList<Season>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Seasons ORDER BY Id";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Season rt = new Season(rs.getString("Name"), rs.getString("StartingDay"),
							Double.toString(rs.getDouble("Price")));
					rt.setId(rs.getInt("Id"));
					seasonsList.add(rt);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seasonsList;

	}

	public Season getSeason(LocalDate date) {
		Season s = null;

		Connector conn = new Connector();
		Connection conection = conn.getConnection();

		String day = String.format("%02d", date.getMonthValue()) + "-" + String.valueOf(date.getDayOfMonth());
		
		String sql = "(SELECT * FROM Seasons WHERE StartingDay <= '" + day
				+ "' ORDER BY StartingDay DESC LIMIT 1)"
				+ "UNION(SELECT * FROM Seasons ORDER BY StartingDay DESC LIMIT 1)" + "ORDER BY StartingDay LIMIT 1;";

		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					s = new Season(rs.getString("Name"), rs.getString("StartingDay"),
							Double.toString(rs.getDouble("Price")));
					s.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return s;
	}

}
