package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import db.Connector;
import models.Customer;

public class CustomerJDBC {

	public void instertCustomer(List<Customer> cust) {

		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting Customers");

		String sql = "INSERT INTO Customers(Email, FirstName, LastName, PhoneNumber,Nationality) VALUES (?,?,?,?,?)";

		try {
			PreparedStatement statement = conection.prepareStatement(sql);

			int counter = 0;
			int limit = 100000;
			int done = 0;
			Iterator<Customer> it = cust.iterator();
			Customer c = null;
			
			while(done!=cust.size()) {
			
			while (it.hasNext() && counter<limit) {
				c = it.next();

				statement.setString(1, c.getEmail());
				statement.setString(2, c.getName());
				statement.setString(3, c.getLastname());
				statement.setString(4, c.getPhoneNumber());
				statement.setString(5, c.getNationality());
				statement.addBatch();
				
				counter++;
			}
			statement.executeBatch();
			done = done + counter;
			System.out.println(done + "/" + cust.size() + " Added into DB, continuing...");
			if(done == cust.size()) {
				counter = cust.size()+1;
			}
			counter = 0;
			}
			statement.close();
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public Customer selectOneCustomerbyEmail(String email) {
		Customer c = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Customers WHERE Email LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setString(1, email);
			
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					c = new Customer(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("PhoneNumber"),rs.getString("Email"),rs.getString("Nationality"));
					c.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}
	public Customer selectOneCustomerbyId(int id) {
		Customer c = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Customers WHERE Id LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, id);
			
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					c = new Customer(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("PhoneNumber"),rs.getString("Email"),rs.getString("Nationality"));
					c.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}
	
	public List<Customer> selectCustomers() {
		List<Customer> customersList = new ArrayList<Customer>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Customers";
		try {
			Statement st = conection.createStatement();
			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Customer c = new Customer(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("PhoneNumber"),rs.getString("Email"),rs.getString("Nationality"));
					c.setId(rs.getInt("Id"));
					customersList.add(c);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customersList;

	}
}
