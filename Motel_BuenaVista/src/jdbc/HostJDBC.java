package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import db.Connector;
import models.Host;

public class HostJDBC {
	
	public void instertHosts(List<Host> hosts) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting Hosts");

		String sql = "INSERT INTO Hosts(DocType, DocNumber, FirstName, LastName, Nationality) VALUES (?,?,?,?,?)";
		
		try {
			PreparedStatement statement = conection.prepareStatement(sql);
			
			
			int counter = 0;
			int limit = 100000;
			int done = 0;
			Iterator<Host> it = hosts.iterator();
			Host h = null;
			
			while(done!=hosts.size()) {
			
			while (it.hasNext() && counter<limit) {
				h = it.next();

				statement.setString(1, h.getDocType());
				statement.setString(2, h.getDocNumber());
				statement.setString(3, h.getName());
				statement.setString(4, h.getLastname());
				statement.setString(5, h.getNationality());
				statement.addBatch();
				
				counter++;
			}
			statement.executeBatch();
			
			done = done + counter;
			System.out.println(done + "/" + hosts.size() + " Added into DB, continuing...");
			if(done == hosts.size()) {
				counter = hosts.size()+1;
			}
			counter = 0;
			}
			statement.close();
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void insertHost(Host h) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting Host");

		String sql = "INSERT INTO Hosts(DocType, DocNumber, FirstName, LastName, Nationality) VALUES (?,?,?,?,?)";
		
		try {
			PreparedStatement statement = conection.prepareStatement(sql);
			
				statement.setString(1, h.getDocType());
				statement.setString(2, h.getDocNumber());
				statement.setString(3, h.getName());
				statement.setString(4, h.getLastname());
				statement.setString(5, h.getNationality());			
				statement.executeQuery();
				statement.close();
				conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public Host selectOneHostBy(int id) {
		Host c = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Hosts WHERE Id LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, id);
			
			

			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					c = new Host(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("Nationality"),rs.getString("DocType"),rs.getString("DocType"));
					c.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}
	
	public List<Host> selectHosts() {
		List<Host> hostsList = new ArrayList<Host>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Hosts";
		try {
			Statement st = conection.createStatement();
			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Host c = new Host(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("Nationality"),rs.getString("DocType"),rs.getString("DocType"));
					c.setId(rs.getInt("Id"));					
					hostsList.add(c);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return hostsList;

	}

}
