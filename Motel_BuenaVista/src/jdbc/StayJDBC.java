package jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.Stay;

public class StayJDBC {

	public void insertStays(List<Stay> stayList) throws SQLException {

		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "INSERT INTO Stays(CheckIn, RoomNumber,CheckOut,TotalPrice,PaymentType,ReservationDateTime,PaymentDateTime,CustomerEmail) VALUES (?, ?,?,?,?,?,?,?)";
		try {

			PreparedStatement statement = conection.prepareStatement(sql);

			for (Stay s : stayList) {
				statement.setDate(1, Date.valueOf(s.getCheckIn()));
				statement.setInt(2, s.getRoomNumber());
				statement.setDate(3, Date.valueOf(s.getCheckOut()));
				statement.setDouble(4, s.getTotalPrice());
				statement.setString(5, s.getPaymanetType());
				statement.setDate(6, Date.valueOf(s.getReservationDateTime()));
				statement.setDate(7, Date.valueOf(s.getPaymentDateTime()));
				statement.setString(8, s.getCustomerEmail());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conection.close();
		}

	}

	public void insertStay(Stay s) throws SQLException {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();

		String sql = "INSERT INTO Stays(CheckIn, RoomNumber,CheckOut,TotalPrice,PaymentType,ReservationDateTime,PaymentDateTime,CustomerEmail) VALUES (?, ?,?,?,?,?,?,?)";
		try {

			PreparedStatement statement = conection.prepareStatement(sql);

				statement.setDate(1, Date.valueOf(s.getCheckIn()));
				statement.setInt(2, s.getRoomNumber());
				statement.setDate(3, Date.valueOf(s.getCheckOut()));
				statement.setDouble(4, s.getTotalPrice());
				statement.setString(5, s.getPaymanetType());
				statement.setDate(6, Date.valueOf(s.getReservationDateTime()));
				statement.setDate(7, Date.valueOf(s.getPaymentDateTime()));
				statement.setString(8, s.getCustomerEmail());
				statement.addBatch();
			
			statement.executeBatch();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conection.close();
		}

	}
	public Stay selectOneStaybyCheckIn(LocalDate date) {
		Stay stay = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Stays WHERE CheckOut LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setDate(1, Date.valueOf(date));
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {

					stay = new Stay(rs.getDate("CheckIn").toLocalDate(), rs.getInt("RoomNumber"),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("TotalPrice"),
							rs.getString("PaymentType"), rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getDate("PaymentDateTime").toLocalDate(), rs.getString("CustomerEmail"));
					stay.setId(rs.getInt("Id"));

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stay;
	}
	public Stay selectOneStaybyCustomerEmail(String email) {
		Stay stay = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Stays WHERE CustomerEmail LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setString(1,email);
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {

					stay = new Stay(rs.getDate("CheckIn").toLocalDate(), rs.getInt("RoomNumber"),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("TotalPrice"),
							rs.getString("PaymentType"), rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getDate("PaymentDateTime").toLocalDate(), rs.getString("CustomerEmail"));
					stay.setId(rs.getInt("Id"));

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stay;
	}
	public List<Stay> selectStays() {
		List<Stay> staysList = new ArrayList<Stay>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Stays";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Stay stay = new Stay(rs.getDate("CheckIn").toLocalDate(), rs.getInt("RoomNumber"),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("TotalPrice"),
							rs.getString("PaymentType"), rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getDate("PaymentDateTime").toLocalDate(), rs.getString("CustomerEmail"));
					stay.setId(rs.getInt("Id"));
					staysList.add(stay);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return staysList;

	}
}
