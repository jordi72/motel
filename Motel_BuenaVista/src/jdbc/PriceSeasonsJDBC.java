package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.PriceSeason;

public class PriceSeasonsJDBC {

	public void insertPriceSeasons(List<PriceSeason> priceSeasons) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting PriceSeasons");

		String sql = "INSERT INTO PriceSeasons (SeasonId,RoomTypeId,Price) VALUES (?,?,?)";

		try {
			PreparedStatement statement = conection.prepareStatement(sql);
			for (PriceSeason s : priceSeasons) {
				statement.setInt(1, s.getSeasonId());
				statement.setInt(2, s.getRoomTypeId());
				statement.setDouble(3, s.getPrice());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public PriceSeason selectOnePriceSeasonById(int id) {
		PriceSeason priceseason = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM PriceSeasons WHERE Id LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, id);

			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					priceseason = new PriceSeason(rs.getInt("SeasonId"),rs.getInt("RoomTypeId"));
					priceseason.setId(rs.getInt("Id"));
				}
			}
			st.close();
			
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return priceseason;

	}
	
	public double getBookingPrice(int roomTypeid, int seasonid) {
		double priceseason = 0;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "select Price FROM PriceSeasons WHERE RoomTypeId = (?) AND SeasonId = (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, roomTypeid);
			st.setInt(2, seasonid);

			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					priceseason = rs.getDouble("Price");
				}
			}
			st.close();
			
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return priceseason;

	}
	
	public List<PriceSeason> selectPriceSeasons() {
		List<PriceSeason> priceseasonsList = new ArrayList<PriceSeason>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM PriceSeasons ORDER BY Id";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					PriceSeason rt = new PriceSeason(rs.getInt("SeasonId"),rs.getInt("RoomTypeId"));
					rt.setId(rs.getInt("Id"));
					priceseasonsList.add(rt);

				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return priceseasonsList;

	}

}
