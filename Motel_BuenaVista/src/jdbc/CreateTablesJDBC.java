package jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import db.Connector;

public class CreateTablesJDBC {
	
	public void createTables(List<String> tables) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();

		try {
			Statement statement = conection.createStatement();
			System.out.println("Generating DataBase, please wait...");
			for(String s : tables) {
				statement.executeUpdate(s);
			}
			System.out.println("DataBase Succesfully Created.\n");
			statement.close();
			conection.close();
		} catch (SQLException e) {
			System.err.println("Something's wrong, database can't be created.");
			e.printStackTrace();
		}
		
	}

}
