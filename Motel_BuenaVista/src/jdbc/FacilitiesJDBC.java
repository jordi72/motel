package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.Facilities;

public class FacilitiesJDBC {
	
	
	
	
	public void instertFacilities(List<Facilities> facilities) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting Facilities");

		String sql = "INSERT INTO Facilities(Name) VALUES (?)";
		
		try {
			PreparedStatement statement = conection.prepareStatement(sql);
			
			for(Facilities f: facilities) {
				statement.setString(1,f.getName());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public Facilities selectOneFacility(String name) {
		Facilities f = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Facilities WHERE Name LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setString(1, name);
			
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					f = new Facilities(rs.getString("Name"));
					f.setId(rs.getInt("Id"));

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		return f;
		
	}
	public List<Facilities> selectFacilities() {
		List<Facilities> facilitiesList = new ArrayList<Facilities>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Facilities ORDER BY Id";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Facilities rt = new Facilities(rs.getString("Name"));
					rt.setId(rs.getInt("Id"));
					facilitiesList.add(rt);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return facilitiesList;

	}

	
}
