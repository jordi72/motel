package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.RoomTypeFacilities;

public class RoomTypeFacilitiesJDBC {

	public void instertRoomTypesFacilities(List<RoomTypeFacilities> roomTypesFacilities) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("Inserting RoomTypeFacilities");

		String sql = "INSERT INTO RoomTypeFacilities (RoomTypeId, FacilityId) VALUES (?,?)";

		try {
			PreparedStatement statement = conection.prepareStatement(sql);

			for (RoomTypeFacilities rtf : roomTypesFacilities) {
				statement.setInt(1, rtf.getRoomTypeId() );
				statement.setInt(2, rtf.getFacilityId());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	
	public List<RoomTypeFacilities> selectRoomTypesFacilities() {
		List<RoomTypeFacilities> roomTypesList = new ArrayList<RoomTypeFacilities>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM RoomTypeFacilities";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					RoomTypeFacilities rt = new RoomTypeFacilities(rs.getInt("RoomTypeId"), rs.getInt("FacilityId"));
					rt.setId(rs.getInt("Id"));
					roomTypesList.add(rt);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return roomTypesList;

	}

}
