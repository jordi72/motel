package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import db.Connector;
import models.StayHost;

public class StayHostsJDBC {
	
	public void instertStayHosts(List<StayHost> stayHosts) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		System.out.println("\nInserting StayHosts\n");

		String sql = "INSERT INTO StayHosts(StayId, HostId) VALUES (?,?)";

		try {
			PreparedStatement statement = conection.prepareStatement(sql);

			for (StayHost sh : stayHosts) {
				statement.setInt(1, sh.getStayId());
				statement.setInt(2, sh.getHostId());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
