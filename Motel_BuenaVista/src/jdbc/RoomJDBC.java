package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.Room;

public class RoomJDBC {

	public void insertRooms(List<Room> roomsList) {
		System.out.println("Inserting Rooms");

		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "INSERT INTO Rooms(RoomTypeId, Empty) VALUES (?,?)";
		try {

			PreparedStatement statement = conection.prepareStatement(sql);

			for (Room rt : roomsList) {
				statement.setInt(1, rt.getRoomType());
				statement.setBoolean(2, rt.getEmpty());
				statement.addBatch();
			}

			statement.executeBatch();
			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public List<Room> selectEmptyRooms(){
		
		List<Room> roomList = new ArrayList<Room>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Rooms WHERE Empty LIKE true";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Room rt = new Room(rs.getInt("RoomNumber"), rs.getBoolean("Empty") ,  rs.getInt("RoomTypeId"));
					roomList.add(rt);
				}
			}
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return roomList;
		
		
	}
	
	public List<Room> selectUsedRooms(){
		
		List<Room> roomList = new ArrayList<Room>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Rooms WHERE Empty LIKE false";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Room rt = new Room(rs.getInt("RoomNumber"), rs.getBoolean("Empty") ,  rs.getInt("RoomTypeId"));
					roomList.add(rt);
				}
			}
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return roomList;
		
		
	}
	public Room selectOneRoombyNumber(int number) {
		Room rt = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Rooms WHERE RoomNumber LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, number);
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					rt = new Room(rs.getInt("RoomNumber"), rs.getBoolean("Empty") ,  rs.getInt("RoomTypeId"));
				}
			}
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rt;
	}
	
	public List<Room> selectRooms() {
		List<Room> roomList = new ArrayList<Room>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Rooms";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Room rt = new Room(rs.getInt("RoomNumber"), rs.getBoolean("Empty") ,  rs.getInt("RoomTypeId"));

					roomList.add(rt);

				}
			}
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return roomList;

	}
	
	public void updateRoomStatus(int roomId,Boolean status) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = " UPDATE Rooms SET Empty = (?) WHERE RoomNumber = (?);";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setBoolean(1, status);
			st.setInt(2, roomId);
			st.executeUpdate();
			st.close();
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
