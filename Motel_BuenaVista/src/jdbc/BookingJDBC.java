package jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import db.Connector;
import models.Booking;

public class BookingJDBC {

	public void insertBookings(List<Booking> bookingList) throws SQLException {
		System.out.println("Inserting Booking");
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "INSERT INTO Booking(ReservationDateTime, CustomerEmail,RoomTypeId,CheckIn,CheckOut,Price,State,NHosts) VALUES (?,?,?,?,?,?,?,?)";
		try {

			PreparedStatement statement = conection.prepareStatement(sql);

			int counter = 0;
			int limit = 100000;
			int done = 0;
			Iterator<Booking> it = bookingList.iterator();
			Booking b = null;

			while (done != bookingList.size()) {

				while (it.hasNext() && counter < limit) {
					b = it.next();
					statement.setDate(1, Date.valueOf(b.getReservationDateTime()));
					statement.setString(2, b.getCustomerEmail());
					statement.setInt(3, b.getRoomTypeId());
					statement.setDate(4, Date.valueOf(b.getCheckIn()));
					statement.setDate(5, Date.valueOf(b.getCheckOut()));
					statement.setDouble(6, b.getPrice());
					statement.setString(7, b.getState());
					statement.setInt(8, b.getNhosts());
					statement.addBatch();

					counter++;
				}
				statement.executeBatch();
				done = done + counter;
				System.out.println(done + "/" + bookingList.size() + " Added into DB, continuing...");
				if (done == bookingList.size()) {
					counter = bookingList.size() + 1;
				}
				counter = 0;
			}
			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conection.close();
		}
	}

	public Booking selectOneBookingbyCEmail(String email) {
		Booking booking = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Booking WHERE CustomerEmail LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setString(1, email);
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					booking = new Booking(rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getString("CustomerEmail"), rs.getInt("RoomTypeId"), rs.getDate("CheckIn").toLocalDate(),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("Price"), rs.getString("State"),
							rs.getInt("NHosts"));
					booking.setId(rs.getInt("Id"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return booking;
	}

	public List<Booking> selectBookingsbyCheckOut(LocalDate checkout) {
		List<Booking> bookingList = new ArrayList<Booking>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM Booking WHERE CheckOut = ?";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setDate(1, Date.valueOf(checkout));
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					Booking booking = new Booking(rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getString("CustomerEmail"), rs.getInt("RoomTypeId"), rs.getDate("CheckIn").toLocalDate(),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("Price"), rs.getString("State"),
							rs.getInt("NHosts"));
					booking.setId(rs.getInt("Id"));
					bookingList.add(booking);
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookingList;
	}

	public List<Booking> selectBookings() {
		List<Booking> bookingList = new ArrayList<Booking>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Booking";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Booking booking = new Booking(rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getString("CustomerEmail"), rs.getInt("RoomTypeId"), rs.getDate("CheckIn").toLocalDate(),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("Price"), rs.getString("State"),
							rs.getInt("NHosts"));
					booking.setId(rs.getInt("Id"));
					bookingList.add(booking);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookingList;

	}

	public List<Booking> selectBookingsNotReserved() {
		List<Booking> bookingList = new ArrayList<Booking>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM Booking WHERE State LIKE 'Reserved'";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					Booking booking = new Booking(rs.getDate("ReservationDateTime").toLocalDate(),
							rs.getString("CustomerEmail"), rs.getInt("RoomTypeId"), rs.getDate("CheckIn").toLocalDate(),
							rs.getDate("CheckOut").toLocalDate(), rs.getDouble("Price"), rs.getString("State"),
							rs.getInt("NHosts"));
					booking.setId(rs.getInt("Id"));
					bookingList.add(booking);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookingList;

	}

	
	public void insertBooking(Booking booking) throws SQLException {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "INSERT INTO Booking(ReservationDateTime, CustomerEmail,RoomTypeId,CheckIn,CheckOut,Price,State,NHosts) VALUES (?,?,?,?,?,?,?,?)";
		try {

			PreparedStatement statement = conection.prepareStatement(sql);

			statement.setDate(1, Date.valueOf(booking.getReservationDateTime()));
			statement.setString(2, booking.getCustomerEmail());
			statement.setInt(3, booking.getRoomTypeId());
			statement.setDate(4, Date.valueOf(booking.getCheckIn()));
			statement.setDate(5, Date.valueOf(booking.getCheckOut()));
			statement.setDouble(6, booking.getPrice());
			statement.setString(7, booking.getState());
			statement.setInt(8, booking.getNhosts());
			statement.addBatch();

			statement.executeBatch();

			statement.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conection.close();
		}
	}

}
