package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.Connector;
import models.RoomType;

public class RoomTypeJDBC {

	public void instertRoomTypes(List<RoomType> roomTypes) {
		Connector conn = new Connector();
		Connection conection = conn.getConnection();

		String sql = "INSERT INTO RoomTypes(Name, Capacity, Shared,BasePrice) VALUES (?,?,?,?)";

		try {
			PreparedStatement statement = conection.prepareStatement(sql);

			for (RoomType rt : roomTypes) {
				statement.setString(1, rt.getName());
				statement.setInt(2, rt.getCapacity());
				statement.setBoolean(3, rt.getShared());
				statement.setInt(4, rt.getPriceBase());
				statement.addBatch();
			}
			System.out.println("Inserting RoomTypes");
			statement.executeBatch();
			statement.close();
			conection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public RoomType selectOneRoomTypebyName(String name) {
		RoomType rt = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM RoomTypes WHERE Name LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setString(1, name);
			
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					rt = new RoomType(rs.getString("Name"), rs.getInt("Capacity"), rs.getBoolean("Shared"),
							rs.getInt("BasePrice"));
					rt.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rt;
	}
	public RoomType selectOneRoomTypebyId(int id) {
		RoomType rt = null;
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "Select * FROM RoomTypes WHERE Id LIKE (?)";

		try {
			PreparedStatement st = conection.prepareStatement(sql);
			st.setInt(1, id);
			
			try (ResultSet rs = st.executeQuery();) {
				while (rs.next()) {
					rt = new RoomType(rs.getString("Name"), rs.getInt("Capacity"), rs.getBoolean("Shared"),
							rs.getInt("BasePrice"));
					rt.setId(rs.getInt("Id"));
				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rt;
	}
	
	public List<RoomType> selectRoomTypes() {
		List<RoomType> roomTypesList = new ArrayList<RoomType>();
		Connector conn = new Connector();
		Connection conection = conn.getConnection();
		String sql = "SELECT * FROM RoomTypes";
		try {
			Statement st = conection.createStatement();

			try (ResultSet rs = st.executeQuery(sql);) {
				while (rs.next()) {
					RoomType rt = new RoomType(rs.getString("Name"), rs.getInt("Capacity"), rs.getBoolean("Shared"),
							rs.getInt("BasePrice"));
					rt.setId(rs.getInt("Id"));
					roomTypesList.add(rt);

				}
			}
			st.close();
			conection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return roomTypesList;

	}

}
