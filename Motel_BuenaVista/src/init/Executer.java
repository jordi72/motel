package init;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;
import core.Core;
import db.Connector;
import db.Dumper;


public class Executer {
	Boolean dbconnection = false;
	private static Scanner sn;

	/**
	 * Here is where start, u need to execute this. Only call Menu method and menu
	 * do everything.
	 */
	public static void main(String[] args) {
		if (tryConnect())
			menu();
		System.out.println("Dumping Database, please wait...");
		if(args.length != 0) {
		dump(args[0]);
		}else {
			dump("MotelBackUp");
		}
	}

	/**
	 * Got a scanner for the menu, after getting the number of customers , call the
	 * core. the parameters is only the customers numbers.
	 * 
	 */
	public static void menu() {

		sn = new Scanner(System.in);
		boolean customerExit = false;
		boolean roomsExit = false;
		int customers = 0;
		int rooms = 0;

		while (!customerExit) {
			System.out.println("Welcome to Motel BuenaVista DB generator.");
			System.out.println("----");
			System.out.println("How many customers do you want?");
			System.out.println(">");
			try {
				customers = sn.nextInt();
				customerExit = true;

			} catch (InputMismatchException e) {
				System.out.println("Only integer.");
				sn.next();
			}
			while (!roomsExit) {
				System.out.println("How many rooms do you want?");
				System.out.println(">");
				try {
					rooms = sn.nextInt();
					@SuppressWarnings("unused")
					Core c = new Core(customers, rooms);
					roomsExit = true;

				} catch (InputMismatchException e) {
					System.out.println("Only integer.");
					sn.next();
				}
			}
		}

	}

	public static boolean tryConnect() {
		Boolean returnBoolean = false;
		try {
			@SuppressWarnings("unused")
			Connector connector = new Connector().initConnection();
			returnBoolean = true;
		} catch (SQLException sql) {
				System.out.println(sql.getMessage());

		} catch (FileNotFoundException fne) {
			System.out.println(fne.getMessage());
		}
		
		return returnBoolean;
	}
	
	
	public static void dump(String f) {
		File file = new File(f + ".sql");
		Dumper dump  = new Dumper();
		try {
			dump.dumpMysql(file);
		} catch (InterruptedException | IOException | SQLException e) {
			e.printStackTrace();
		}
	}
	

}
