package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.io.FileNotFoundException;
import java.util.List;

import readers.Reader;

public class NationalityGenerator {
	private List<String> nationalities;
	
	/**
	 * At the constructor only add two Nationalities for the moment.
	 * Feel free to add anything.
	 * Its a List of Strings only, but remember at the HostFactory add a new equal
	 * See the GitLab for more info.
	 * 
	 * https://gitlab.com/jordi72/motel
	 */
	
	
	public NationalityGenerator() {
		Reader reader = new Reader();
		try {
			nationalities = reader.nationalities();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Return a random nationality 
	 * @return one String with the nationality
	 */

	public String getNewNationality() {
		return nationalities.get(current().nextInt(nationalities.size()));
	}
}
