package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import readers.Reader;

public class NameGenerator {
	
	Map<String, String> completeNames = new HashMap<String, String>();
	
	List<String> names;
	List<String> lastnames;
	List<String> nombres;
	List<String> apellidos;
	
	
	/**
	 * Call readers for getting the names.
	 * 
	 */
	public NameGenerator(){
	
		try {
			Reader reader = new Reader();
			nombres = reader.spanishNames();
			apellidos = reader.spanishLastNames();
			names = reader.englishNames();
			lastnames = reader.englishLastNames();
			
		}catch(FileNotFoundException ex) {
			ex.getMessage();
		}
	}

	
	
	/**
	 * Return a random name of the nationality
	 * @param String with the name
	 * @return one String with the name
	 */
	public String getName(String nationality) {
		if(nationality.equals("Spanish"))
			return nombres.get(current().nextInt(nombres.size())).toUpperCase();
		return names.get(current().nextInt(names.size())).toUpperCase();
	}
	/**
	 * Return a random lastname of the nationality
	 * @param String with the lastname
	 * @return one String with the lastname
	 */

	public String getLastName(String nationality) {
		if(nationality.equals("Spanish"))
			return apellidos.get(current().nextInt(apellidos.size())).toUpperCase() + " " + apellidos.get(current().nextInt(apellidos.size())).toUpperCase();
		return lastnames.get(current().nextInt(this.lastnames.size())).toUpperCase();
	}
	/**
	 * Return a random domain for the email of the nationality
	 * @param String with the nationality
	 * @return one String with the domain for the email.
	 */

	public String getDomain(String nationality) {
		if(nationality.equals("Spanish"))
			return "es";
		return "co.uk";
	}

}
