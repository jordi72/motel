package generators;

import java.util.ArrayList;
import java.util.List;

import jdbc.RoomTypeJDBC;
import jdbc.SeasonsJDBC;
import models.PriceSeason;
import models.RoomType;
import models.Season;

public class PriceSeasonsGenerator {

	public PriceSeasonsGenerator() {
	}
	
	public List<PriceSeason> createPriceSeasons() {
		List<PriceSeason> lps = new ArrayList<>();
		SeasonsJDBC sjdbc = new SeasonsJDBC();
		List<Season> ls = sjdbc.selectSeasons();
		RoomTypeJDBC rtjdbc = new RoomTypeJDBC();
		List<RoomType> lrt = rtjdbc.selectRoomTypes();
	
		
		for(RoomType rt :lrt) {
			for(Season s : ls) {
				PriceSeason ps = new PriceSeason(rt.getId(),s.getId());
				lps.add(ps);
			}
		}
		return lps;
	}
}
