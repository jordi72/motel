package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.io.FileNotFoundException;
import java.text.Normalizer;
import java.util.List;
import models.Customer;
import readers.Reader;

public class EmailGenerator {

	/**
	 * Return a random email.
	 * 
	 * @param Customer
	 *            customer.
	 * @return one String with the random generated email...
	 */

	public String getEmail(Customer customer) {

		Reader reader = new Reader();

		List<String> domains = null;
		try {
			domains = reader.emails();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String email = "";

		String domain = domains.get(current().nextInt(domains.size()));
		String nameReplaced = normalizer(customer.getName());
		String lastnameReplaced = normalizer(customer.getLastname());

		email = nameReplaced.replace(" ", "") + "_" + lastnameReplaced.replace(" ", "") + "@" + domain + "."
				+ customer.getDot();

		return email;
	}

	public String normalizer(String s) {
		return Normalizer.normalize(s.toLowerCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").replace("'",
				"");
	}

}
