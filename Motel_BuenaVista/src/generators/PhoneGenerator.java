package generators;

import static java.util.concurrent.ThreadLocalRandom.current;


import models.Customer;

public class PhoneGenerator {
	
	/**
	 * Return a random phone based of the nationality
	 * @param Customer
	 * @return one String with the number + prefix
	 */
	
	public String getPhone(Customer customer) {
		
		
		String number = null;
		String prefixUK = "+44";
		String prefixSpain = "+34";
		
		String tempNumber = "";
		
		for(int i = 0 ; i < 9 ; i++) {
			tempNumber = tempNumber + String.valueOf(current().nextInt(10));
		}
		if(customer.getNationality().equals("Spanish")) {
			number = prefixSpain + tempNumber;
		}else if(customer.getNationality().equals("English")) {
			number = prefixUK + tempNumber;
		}
	
		return number;
	}

}
