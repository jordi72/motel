package generators;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.List;

import jdbc.SeasonsJDBC;
import models.Season;
import readers.Reader;

public class SeasonsGenerator {

	List<Season> seasons;

	/**
	 * Return a List with all facilities. for insert to the DB, Hard-Coded
	 * 
	 * @return One list of Facilities
	 */

	public SeasonsGenerator() {
		Reader reader = new Reader();
		try {
			seasons = reader.seasons();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Season> getSeasons() {
		return seasons;
	}

	public Season getSeason(LocalDate l) {
		SeasonsJDBC sjdbc = new SeasonsJDBC();
		List<Season> listofseasons = sjdbc.selectSeasons();
		Season seasonReturn = null;
		
		LocalDate before = LocalDate.of(l.getYear(), 9, 21);
		
		
		for (Season s : listofseasons) {
			if (l.isEqual(s.getLocalDate().withYear(l.getYear()))) {
				seasonReturn = s;
			} else {
				if (l.isBefore(before.withYear(l.getYear())) && l.isAfter(s.getLocalDate().withYear(l.getYear()))) {
					seasonReturn = s;
				} else {
					before = s.getLocalDate().withYear(l.getYear());
				}
			}
		}
		return seasonReturn;

	}	
}
