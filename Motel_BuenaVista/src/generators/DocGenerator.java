package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.util.HashMap;
import java.util.Map;

public class DocGenerator {

	Map<Integer, Character> DNILetter = new HashMap<Integer, Character>();

	public DocGenerator() {
		/**
		 * Here insert into a MAP the spanish DNI letters
		 * 
		 */

		DNILetter.put(0, 'T');
		DNILetter.put(1, 'R');
		DNILetter.put(2, 'W');
		DNILetter.put(3, 'A');
		DNILetter.put(4, 'G');
		DNILetter.put(5, 'M');
		DNILetter.put(6, 'Y');
		DNILetter.put(7, 'F');
		DNILetter.put(8, 'P');
		DNILetter.put(9, 'D');
		DNILetter.put(10, 'X');
		DNILetter.put(11, 'B');
		DNILetter.put(12, 'N');
		DNILetter.put(13, 'J');
		DNILetter.put(14, 'Z');
		DNILetter.put(15, 'S');
		DNILetter.put(16, 'Q');
		DNILetter.put(17, 'V');
		DNILetter.put(18, 'H');
		DNILetter.put(19, 'L');
		DNILetter.put(20, 'C');
		DNILetter.put(21, 'K');
		DNILetter.put(22, 'E');
	}

	/**
	 * Return the type of doc. DNI for spanish or Passport to english
	 * 
	 * @param nationality
	 *            is a String where contains the nationality
	 * @return one String of the random doc. (For example, the DNI 32154231W)
	 */
	public String getNationality(String nationality) {
		if (nationality.equals("Spanish"))
			return getDNI();
		return getPassport();
	}

	/**
	 * Return the document type for the nationality
	 * 
	 * @param String
	 *            nationality Where contains the nationality
	 * @return one String with the doctype.
	 */
	public String getDoctype(String nationality) {
		if (nationality.equals("Spanish"))
			return "DNI";
		return "Passport";
	}

	/**
	 * Return a random DNI example.
	 * 
	 * @return one String with the doc number for Spanish people..
	 */

	public String getDNI() {

		String dniNum = "";
		Character letter = null;

		for (int i = 0; i < 8; i++) {
			dniNum = dniNum + String.valueOf(current().nextInt(10));
		}
		// Will be used to calculate the letter.
		int residue = Integer.valueOf(dniNum) % 23;

		// Looking for the residue on the Map filled in Builder
		for (Map.Entry<Integer, Character> entry : DNILetter.entrySet()) {
			Integer key = entry.getKey();
			Character value = entry.getValue();

			if (residue == key.intValue())
				letter = value.charValue();
			continue;
		}
		// returning the complete DNI (Identity National Document)
		return dniNum + "-" + String.valueOf(letter);
	}

	/**
	 * Return a random passport example.Random numbers with 8-9 length.
	 * 
	 * @return one String with the doc number for english people.
	 */
	public String getPassport() {
		String passport = "";

		for (int i = 0; i < current().nextInt(2) + 8; i++) {
			passport = passport + String.valueOf(current().nextInt(10));
		}
		return String.valueOf(passport);
	}

}
