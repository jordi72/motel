package generators;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import jdbc.FacilitiesJDBC;
import jdbc.RoomTypeJDBC;
import models.Facilities;
import models.RoomType;
import models.RoomTypeFacilities;
import readers.Reader;

public class RoomTypeFacilitiesGenerator {

	
	public RoomTypeFacilitiesGenerator() {
	}
	
	public List<RoomTypeFacilities> getList() {
		List<RoomTypeFacilities> returnList = new ArrayList<>();
		Reader reader = new Reader();
		try {
			List<String[]> listOfFacilities = reader.roomTypesFacilities();
			
			FacilitiesJDBC fjdbc = new FacilitiesJDBC();
			RoomTypeJDBC rtjdbc = new RoomTypeJDBC();

			for(String[] arrayS : listOfFacilities) {
				String nameOfRoomType = arrayS[0];
				String nameOfFacility = arrayS[1];
				Facilities facility = fjdbc.selectOneFacility(nameOfFacility);
				RoomType roomType  =  rtjdbc.selectOneRoomTypebyName(nameOfRoomType);
				returnList.add(new RoomTypeFacilities(roomType.getId(),facility.getId()));							
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnList;
	}
	
	
}
