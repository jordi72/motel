package generators;

import java.io.FileNotFoundException;
import java.util.List;

import models.Facilities;
import readers.Reader;

public class FacilityGenerator {

	List<Facilities> facilities;
	/**
	 * Return a List with all facilities. for insert to the DB, Hard-Coded
	 * 
	 * @return One list of Facilities
	 */
	
	public FacilityGenerator() {
		Reader reader = new Reader();
		try {
			facilities = reader.facilities();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public List<Facilities> getFacilities() {
		return facilities;
	}



}
