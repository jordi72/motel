package core;


import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import factorys.BookingFactory;
import factorys.CustomerFactory;
import factorys.RoomFactory;
import factorys.StayHostsFactory;
import generators.FacilityGenerator;
import generators.PriceSeasonsGenerator;
import generators.RoomTypeFacilitiesGenerator;
import generators.SeasonsGenerator;
import jdbc.CreateTablesJDBC;
import jdbc.CustomerJDBC;
import jdbc.FacilitiesJDBC;
import jdbc.PriceSeasonsJDBC;
import jdbc.RoomJDBC;
import jdbc.RoomTypeFacilitiesJDBC;
import jdbc.RoomTypeJDBC;
import jdbc.SeasonsJDBC;
import jdbc.StayHostsJDBC;
import models.Customer;
import models.Facilities;
import models.PriceSeason;
import models.Room;
import models.RoomType;
import models.RoomTypeFacilities;
import models.Season;
import readers.Reader;

public class Core {
	int customers;
	int rooms;
	List<Customer> customerList;
	/**
	 * Customer got a field for the numbers of customers. create Factorys.
	 * 
	 */
	public Core(int customers,int rooms) {
		super();
		this.customers = customers;
		this.rooms = rooms;
		createTables();
		customerList = customersCore(customers);
		roomTypeCore();
		facilitiesCore();
		roomTypeFacilities();
		seasonCore();
		priceSeasonCore();
		roomsCore();
		bookingCore();
		stayHostsCore();
		System.out.println("Database Succesfully Generated!\n");
	}

	public List<Customer> customersCore(int customers) {
		List<Customer> returnList = null;
		System.out.println("Generating Customers\n");
		try {
			CustomerFactory cf = new CustomerFactory();
			returnList = cf.createCustomers(customers);
			CustomerJDBC customerJdbc = new CustomerJDBC();
			System.out.println("Finished Customers Generation\n");
			customerJdbc.instertCustomer(returnList);
			System.out.println("Finished Customer Insertation\n");

		} catch (Exception e) {
			System.out.println("Something's wrong with a Customer");
			e.printStackTrace();
		}
		return returnList;
	}
	public List<RoomType> roomTypeCore() {
		List<RoomType> returnList = null;

		try {
			Reader reader = new Reader();
			returnList = reader.roomTypes();
			RoomTypeJDBC roomTypeJdbc = new RoomTypeJDBC();
			roomTypeJdbc.instertRoomTypes(returnList);

		} catch (Exception e) {

		}
		return returnList;
	}

	public List<Facilities> facilitiesCore() {
		List<Facilities> returnList = null;

		try {
			FacilityGenerator fg = new FacilityGenerator();
			returnList = fg.getFacilities();
			FacilitiesJDBC facilityJdbc = new FacilitiesJDBC();
			facilityJdbc.instertFacilities(returnList);

		} catch (Exception e) {

		}
		return returnList;
	}

	public List<Season> seasonCore() {
		List<Season> returnList = new ArrayList<>();

		try {
			SeasonsGenerator sg = new SeasonsGenerator();
			SeasonsJDBC seasonsjdbc = new SeasonsJDBC();
			seasonsjdbc.insertSeasons(sg.getSeasons());
		} catch (Exception e) {

		}
		return returnList;
	}

	public List<PriceSeason> priceSeasonCore() {
		List<PriceSeason> returnList = null;
		try {
			PriceSeasonsGenerator psg = new PriceSeasonsGenerator();

			PriceSeasonsJDBC psjdbc = new PriceSeasonsJDBC();
			psjdbc.insertPriceSeasons(psg.createPriceSeasons());

		} catch (Exception e) {

		}
		return returnList;
	}

	public List<RoomTypeFacilities> roomTypeFacilities() {
		List<RoomTypeFacilities> returnList = null;
		RoomTypeFacilitiesGenerator rtfg = new RoomTypeFacilitiesGenerator();
		returnList = rtfg.getList();
		RoomTypeFacilitiesJDBC rtfJDBC = new RoomTypeFacilitiesJDBC();
		rtfJDBC.instertRoomTypesFacilities(returnList);
		return returnList;

	}

	public List<Room> roomsCore(){
		List<Room> returnList = null;
		try {
			RoomFactory rf = new RoomFactory();
			RoomJDBC rjdbc = new RoomJDBC();
			rjdbc.insertRooms(rf.createRooms(rooms));
		} catch (Exception e) {

		}
		return returnList;
	}
	public void createTables() {
		Reader reader = new Reader();
		try {
			String[] tables = reader.createTables().split(";");
			List<String> finalTables = new ArrayList<>();
			for (String s : tables) {
				finalTables.add(s + ";");
			}
			CreateTablesJDBC ct = new CreateTablesJDBC();
			ct.createTables(finalTables);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void bookingCore() {
		BookingFactory booking = BookingFactory.getInstance();
		CustomerJDBC cjdbc = new CustomerJDBC();
		List<Customer> customerList = cjdbc.selectCustomers();
		try {
			booking.generateBooking(customerList);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void stayHostsCore() {
		StayHostsFactory shf = new StayHostsFactory();
		StayHostsJDBC shjdbc = new StayHostsJDBC();
		shjdbc.instertStayHosts(shf.createStayHosts());
	}
}
