# Motel Bona Vista.
# Que és? (Català)
*Motel BuenaVista* és un projecte fet amb Java i la tecnologia JDBC la qual et genera una base de dades open-source amb dades coherents.
És un projecte open-source, pots modificar i millorar el projecte.

# Que es? (Español)
*Motel BuenaVista* es un proyecto hecho con Java y la tecnología JDBC la cual te genera una base de datos open-source con datos coherentes.
Es un proyecto open-source, puedes modificar y mejorar el proyecto.


# About (English)
*Motel BuenaVista* is a project maded in Java and the JDBC technology which generates an open-source database with coherent data.
It is a project open-source, can modify and improve the project.

==============================

>Diagram:
![Imgur](https://i.imgur.com/B8ECy9X.png)

# Architecture (Català-Español-English)
CA: El projecte està dividit per paquets.

ES: El proyecto está dividido en paquetes:

EN: The project is splited by packages:

![Imgur](https://i.imgur.com/89gzgFX.png)

>INIT

![Imgur](https://i.imgur.com/5Q2rdpK.png)

CA: Aquí hi ha el Main, que executa el Core amb els paràmetres que demana al
menú.

ES: Aquí hay el Main, que ejecuta el Core con los parámetros que pide al
menú.

EN:  Here is the Main, that executes the Core with the parameters that asks to the menu

>CORE

![Imgur](https://i.imgur.com/icEs4zK.png)

CA: El core és un arxiu el qual crida i genera tot, Crida a les factories (per fer Customers, Hosts entre d'altres ) i també les insereix mitjançant classes i mètodes del paquet JDBC.

ES: El core es un archivo el cual llama y genera todo, Llama a las factorías (para hacer Customers, Hosts entre otros ) y también las inserta mediante clases y métodos del paquete JDBC.

EN: The core is a file which call and generates everything, Call the factories (to do Customers, Hosts etc ) and also inserts them using classes and methods of the package JDBC.

>DB

![Imgur](https://i.imgur.com/7Gbmb7n.png)

CA: Classe per agafar els connectors per JDBC, en aquest cas només està per MySQL, es pot ampliar amb més gestors de bases de dades.

ES: Clase para coger los conectores por JDBC, en este caso sólo está por MySQL, se puede ampliar con más gestores de bases de datos.

EN: Class to take the connectors for JDBC, in this case is only applies for MySQL, can be expanded with more database managers.

>FACTORYS

![Imgur](https://i.imgur.com/Tu1kgNc.png)

CA: Es criden al Core i fan la funció de crear X objectes del tipus que necessitis, fan servir molts Generators.

ES: Se llaman al Core y hacen la función de crear X objetos del tipo que necesites, usan muchos Generators.

EN: Call by the Core and do the function to create X objects of the type that needed, use many Generators.

>GENERATORS

![Imgur](https://i.imgur.com/3NaFvFj.png)

CA: Aquí tenim els generadors, tenim per exemple NameGenerator que és la més intuïtiva, et genera cognoms i noms depèn de nacionalitat i sexe, agafant els arxius de la carpeta data.

ES: Aquí tenemos los generadores, tenemos por ejemplo NameGenerator que es la más intuitiva, te genera apellidos y nombres depende de nacionalidad y sexo, cogiendo los archivos de la carpeta data.

EN: Here have the generators,  for example NameGenerator that it is the most intuitive, generates you surnames and names based of nationality and sex, taking the files of the folder data.

>JDBC

![Imgur](https://i.imgur.com/PRcW3Rt.png)

CA: Aquí hi ha les classes per fer INSERT, SELECT o UPDATES d'objectes, normalment demanen una List per fer un INSERT, SELECT o UPDATE en cas de ser variïs objectes o directament un objecte en cas de ser només un.

ES: Aquí hay las clases para hacer INSERT, SELECT o UPDATES de objetos, normalmente piden una List para hacer un INSERT, SELECT o UPDATE en caso de ser varíes objetos o directamente un objeto en caso de ser sólo uno.

EN: Here are the classes to do INSERT, SELECT or UPDATES of objects, usually ask one List to do one INSERT, SELECT or UPDATE in case to be a lot of objects or directly an object in case to be only one

>MODELS

![Imgur](https://i.imgur.com/Gb0K35Z.png)

CA: Són els models dels objectes que tenen una correspondència directa a la BBDD.

ES: Son los modelos de los objetos que tienen una correspondencia directa a la BBDD.

EN: Are the models of the objects that have a direct correspondence to the BBDD.

>READERS

![Imgur](https://i.imgur.com/cEKie8S.png)

CA: Els lectors dels CSV i de la configuració.

ES: Los lectores de los CSV y de la configuración.

EN: The CSV and configuration readers.

==============================

# How install and configurate the Motel BonaVista
> Español.

[![YOUTUBE LINK](https://img.youtube.com/vi/payctugDip4/0.jpg)](https://www.youtube.com/watch?v=payctugDip4)
